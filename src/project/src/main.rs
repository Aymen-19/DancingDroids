/* je voudrais vous demander des excuses, par ce que je n ai pas pu terminer mon projet car je n avait pas les connaissances suffusantes pour cela  j espere prouver mieux que sa dans les projets suivants  */

#[derive(Debug)]

enum Directions {
    N,
    E,
    W,
    S,
}


// structure of a robot.
struct Robot{
    posx : i32,
    posy : i32,
    hit  : bool,   //concerne la collision entre les robots
    direction : char,
}

enum Intructions{
    Right,
    Left,
    Moving,
}

impl Robot{
    fn new(posx: i32, posy: i32, hit: bool, direction: char) -> Robot{
        Robot{posx, posy, hit, direction}
    }
    
    fn my_position(&self){
        println!("Hi there my position is ({:?},{:?}) and oriented to {:?}",self.posx,self.posy, self.direction);     
    }
}




/* for_right est une fonction qui change l'etat de la direction du robot ver l'adroite a chaque fois (pas totalement finie  */

fn for_right (mut state: char) -> char{

    match state{
        'N' => state = 'E',
        'E' => state = 'S',
        'S' => state = 'W',
        'W' => state = 'N',
        _  => println!("false")
    }
    return state;
}

fn for_left(mut state: char) -> char {

    match state{
        'N' => state = 'W',
        'W' => state = 'S',
        'S' => state = 'E',
        'E' => state = 'N',
        _  => println!("false")
    }
    return state;
}

/* cette fonction permet de changer la position du robot elle prend en parametre la position du robot et sa direction, pour donner une nouvelle position comme retour*/

fn move_me(mut px: i32, mut py: i32, direction: char) -> (i32, i32) {
   /*on change l une des deux variables concernees de la position (x ou y) selon la direction du robot */ 
    match direction{
        'N' => py-=1,
        'S' => py+=1,
        'W' => px-=1,
        'E' => px+=1,
         _   => println!("false")
    }
    return (px,py);
}

/* la fonction main contien deux teste pour les fonctions precedente avec le resultat attendu */

fn main() {
/*on commence par creer la structure qui contient nos deux robots et ensuite on effectue les testes*/    
    let mut robots = Vec::new();
    robots.push(Robot::new(0, 2, false,'E'));
    robots.push(Robot::new(4, 2, false, 'N'));
    /* quelques testes pour les fonctions précédantes*/
    robots[0].my_position();
    println!("ma nouvelle direction ver l'adroit {:?}", for_right(robots[1].direction)); 
    println!("ma nouvelle direction ve la gauche {:?}", for_left(robots[1].direction));
    println!("move sur les x {:?}",move_me(robots[0].posx, robots[0].posy, robots[0].direction));
    println!("move sur les y {:?}",move_me(robots[1].posx, robots[1].posy, robots[1].direction));
}
